﻿using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Rocket : MonoBehaviour
{ 
    [SerializeField] private int _energyTotal = 100;
    [SerializeField] private int _energyApply = 50;

    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _flySpeed;
    
    [SerializeField] private bool _collisionOff = false;

    [SerializeField] private TMP_Text _energyText;
    
    [SerializeField] private Rigidbody _rigidBody;
    [SerializeField] private AudioSource _audioSource;
    
    [SerializeField] private AudioClip _flySound;
    [SerializeField] private AudioClip _deadSound;
    [SerializeField] private AudioClip _finishSound;
    [SerializeField] private AudioClip _godemodeSound;

    [SerializeField] private ParticleSystem _flyParticle;
    [SerializeField] private ParticleSystem _deadParticle;
    [SerializeField] private ParticleSystem _finishParticle;
    [SerializeField] private ParticleSystem _godemodeParticle;
    
    //[SerializeField] private Renderer _rocket;


    enum State
    {
        Play,
        Dead,
        NextLevel
    }
    
    private State _state = State.Play;
    
    void Start()
    {
        _godemodeParticle.Stop();
        _state = State.Play;
        _energyText.text = _energyTotal.ToString();
    }

    private void Update()
    {
        if (_state == State.Play || _energyTotal > 0)
        {
            Lounch();
            Rotation();
            DebugKeys();
        }
    }

    private void DebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            _collisionOff = !_collisionOff;
            _audioSource.PlayOneShot(_godemodeSound);
            if (_collisionOff)
                _godemodeParticle.Play();
            else
                _godemodeParticle.Stop();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_state == State.Dead || _state == State.NextLevel || _collisionOff )
        {
            return;
        }
        switch (collision.gameObject.tag)
        {
            case "Friendly":
                print("Friendly");
                break;
            case "Energy":
                PlusEnergy(1000, collision.gameObject);
                break;
            case "Finish":
                Finish();
                break;
            default:
                Lose();
                break;
        }
    }

    private void Lounch()
    {
        _flyParticle.Stop();
        
        if (Input.GetKey(KeyCode.Space) && _energyTotal > 0)
        {
            _energyTotal -= Mathf.RoundToInt(_energyApply * Time.deltaTime) ;
            _energyText.text = _energyTotal.ToString();
            
            _rigidBody.AddRelativeForce(Vector3.up * _flySpeed * Time.deltaTime);
            if (!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_flySound);
            _flyParticle.Play();
        }
        else
        {
            _audioSource.Pause();
            _flyParticle.Stop();
        }
    }
    
    private void Finish()
    {
        _state = State.NextLevel;
        _audioSource.Stop();
        _audioSource.PlayOneShot(_finishSound);
        _finishParticle.Play();
        _flyParticle.Stop();
        Invoke("LoadNextLevel",1f);
    }

    private void Lose()
    {
        _state = State.Dead;
        _audioSource.Stop();
        _audioSource.PlayOneShot(_deadSound);
        _deadParticle.Play();
        _flyParticle.Stop();
        //Destroy(_rocket);
        Invoke("LoadFirstLevel",2f);
    }

    private void LoadNextLevel() //finish
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }
        SceneManager.LoadScene(nextSceneIndex);
    }

    private void LoadFirstLevel() //lose
    {
        SceneManager.LoadScene(0);
    }
    
    private void Rotation()
    {
        float rotSpeed = _rotationSpeed * Time.deltaTime;
        
        _rigidBody.freezeRotation = true;
        
        if (Input.GetKey((KeyCode.A)))
        {
            transform.Rotate(Vector3.forward * rotSpeed);
        }
        else if (Input.GetKey((KeyCode.D)))
        {
            transform.Rotate(-Vector3.forward * rotSpeed);
        }
        
        _rigidBody.freezeRotation = false;
    }

    private void PlusEnergy(int energyToAdd, GameObject battery)
    {
        _energyTotal += energyToAdd;
        _energyText.text = _energyTotal.ToString();
        Destroy(battery);
    }
}
