using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TitleAnimation : MonoBehaviour
{
    [SerializeField] private Transform title;
    
    [SerializeField] private Vector3[] waypoints;
    [SerializeField] private float duration;
    [SerializeField] private PathType pathType;
    [SerializeField] private PathMode pathMode;
    [SerializeField] private int resolution;
    [SerializeField] private Color gizmoColor;

    void Start()
    {
        title.DOPath(waypoints, duration, pathType, pathMode, resolution, gizmoColor);
    }

}
