using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextBlinker : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    
    void Start()
    {
        Invoke("StartBlink", 1.7f);
    }

    private IEnumerator Blink()
    {
        while (true)
        {
            switch (_text.color.a.ToString())
            {
                case "0":
                    _text.color = new Color(_text.color.r,_text.color.g,_text.color.b, 1);
                    yield return new WaitForSeconds(0.5f);
                    break;
                case "1":
                    _text.color = new Color(_text.color.r,_text.color.g,_text.color.b, 0);
                    yield return new WaitForSeconds(0.5f);
                    break;
            }
        }
    }

    private void StartBlink()
    {
        StopCoroutine("Blink");
        StartCoroutine("Blink");
    }

    private void StopBlinking()
    {
        StopCoroutine("Blink");
    }
}
