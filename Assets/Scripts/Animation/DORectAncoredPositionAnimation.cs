﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Animation
{
    public class DORectAncoredPositionAnimation : DOAnimation
    {
        [SerializeField] private RectTransform _target;
        [SerializeField] private Vector2 _from;
        [SerializeField] private Vector2 _to;
        [SerializeField] private DOAnimationSettings _settings;
        
        [SerializeField] private DOAnimation _showAnimation;

        private Tween _tween;

        private void Start()
        {
            _showAnimation.ResetValues();
            _showAnimation.Play();
        }

        public override void Play(Action onEnd = null)
        {
            base.Play(onEnd);

            Animate(_to, onEnd);
        }

        public override void Rewind(Action onEnd = null)
        {
            base.Rewind(onEnd);
            
            Animate(_from, onEnd);
        }

        public override void ResetValues()
        {
            base.ResetValues();

            Stop();
            
            _target.anchoredPosition = _from;
        }

        public override void Stop()
        {
            base.Stop();

            if (_tween != null)
            {
                DOTween.Kill(_tween);
            }
        }
        
        private void Animate(Vector2 to, Action onEnd)
        {
            Stop();

            _tween = _target.DOAnchorPos(to, _settings.Duration);
            
            if (_settings.Delay > 0)
            {
                _tween.SetDelay(_settings.Delay);
            }

            if (_settings.UseCurve)
            {
                _tween.SetEase(_settings.Curve);
            }
            else
            {
                _tween.SetEase(_settings.Ease);
            }

            _tween.OnComplete(() => { onEnd?.Invoke(); });
        }
        
        public override void InstantStart()
        {
            base.InstantStart();
            
            Stop();

            ResetValues();
        }

        public override void InstantEnd()
        {
            base.InstantEnd();
            
            Stop();
            
            _target.anchoredPosition = _to;
        }
    }
}