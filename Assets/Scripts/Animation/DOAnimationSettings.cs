﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Animation
{
    [Serializable]
    public class DOAnimationSettings
    {
        [Header("Base")]
        [SerializeField] private float _duration;
        [SerializeField] private float _delay;

        [Header("Ease")]
        [SerializeField] private bool _useCurve;
        [SerializeField] private Ease _ease;
        [SerializeField] private AnimationCurve _curve;

        public float Duration => _duration;
        public float Delay => _delay;
        public bool UseCurve => _useCurve;
        public Ease Ease => _ease;
        public AnimationCurve Curve => _curve;
    }
}