﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Animation
{
    public class DOCanvasGroupAnimation : DOAnimation
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private float _from;
        [SerializeField] private float _to;
        [SerializeField] private DOAnimationSettings _settings;

        private Tween _tween;
        
        public override void Play(Action onEnd = null)
        {
            base.Play(onEnd);

            Animate(_to, onEnd);
        }

        public override void Rewind(Action onEnd = null)
        {
            base.Rewind(onEnd);
            
            Animate(_from, onEnd);
        }

        public override void ResetValues()
        {
            base.ResetValues();

            Stop();
            
            _canvasGroup.alpha = _from;
        }

        public override void Stop()
        {
            base.Stop();

            if (_tween != null)
            {
                DOTween.Kill(_tween);
            }
        }
        
        private void Animate(float to, Action onEnd)
        {
            Stop();

            _tween = _canvasGroup.DOFade(to, _settings.Duration);
            
            if (_settings.Delay > 0)
            {
                _tween.SetDelay(_settings.Delay);
            }

            if (_settings.UseCurve)
            {
                _tween.SetEase(_settings.Curve);
            }
            else
            {
                _tween.SetEase(_settings.Ease);
            }

            _tween.OnComplete(() => { onEnd?.Invoke(); });
        }

        public override void InstantStart()
        {
            base.InstantStart();
            
            Stop();

            ResetValues();
        }

        public override void InstantEnd()
        {
            base.InstantEnd();
            
            Stop();
            
            _canvasGroup.alpha = _to;
        }
    }
}