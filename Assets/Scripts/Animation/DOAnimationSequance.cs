﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Animation
{
    public class DOAnimationSequance : DOAnimation
    {
        [SerializeField] private float _delay;
        [SerializeField] private DOAnimation[] _animations;

        public override void Play(Action onEnd = null)
        {
            base.Play(onEnd);

            if (_delay > 0)
            {
                for (var i = 0; i < _animations.Length; i++)
                {
                    var anim = _animations[i];
                    var i1 = i;
                    DOVirtual.DelayedCall(i * _delay, () =>
                    {
                        anim.Play(i1 == _animations.Length - 1 ? onEnd : null);
                    });
                }
            }
            else
            {
                for (var i = 0; i < _animations.Length; i++)
                {
                    var anim = _animations[i];
                    anim.Play(i == 0 ? onEnd : null);
                }
            }
        }

        public override void Rewind(Action onEnd = null)
        {
            base.Rewind(onEnd);
            
            if (_delay > 0)
            {
                for (var i = 0; i < _animations.Length; i++)
                {
                    var anim = _animations[i];
                    var i1 = i;

                    DOVirtual.DelayedCall((_animations.Length - i - 1) * _delay, () =>
                    {
                        anim.Rewind(i1 == 0 ? onEnd : null);
                    });
                }
            }
            else
            {
                for (var i = 0; i < _animations.Length; i++)
                {
                    var anim = _animations[i];
                    anim.Rewind(i == 0 ? onEnd : null);
                }
            }
        }

        public override void Stop()
        {
            base.Stop();
            
            foreach (var doAnimation in _animations)
            {
                doAnimation.Stop();
            }
        }

        public override void ResetValues()
        {
            base.ResetValues();
            
            foreach (var doAnimation in _animations)
            {
                doAnimation.ResetValues();
            }
        }

        public override void InstantStart()
        {
            base.InstantStart();
            
            foreach (var doAnimation in _animations)
            {
                doAnimation.InstantStart();
            }
        }

        public override void InstantEnd()
        {
            base.InstantEnd();
            
            foreach (var doAnimation in _animations)
            {
                doAnimation.InstantEnd();
            }
        }
    }
}