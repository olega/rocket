﻿using System;
using UnityEngine;

namespace Animation
{
    public class DOAnimation : MonoBehaviour
    {
        public virtual void Play(Action onEnd = null)
        {
            
        }

        public virtual void Rewind(Action onEnd = null)
        {
            
        }

        public virtual void Stop()
        {
            
        }

        public virtual void ResetValues()
        {
            
        }

        public virtual void InstantEnd()
        {
            
        }

        public virtual void InstantStart()
        {
            
        }
    }
}