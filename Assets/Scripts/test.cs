using System;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;

public class test : MonoBehaviour
{
    public Transform target;
    //public Transform someTransform;

    //public Vector3[] waypoints;
    //public Vector3 lookTarget;
    //public Vector3 forwardDirection;
    //public Vector3 up;
    //public Vector3 punch;
    public Vector3 by;
    public Vector3 anotherBy;




    //public SpriteRenderer sprite;

    //public TrailRenderer trail;


    //public Color gizmoColor;

    //public Gradient gradient;
    
    //ДотТвиновские переменные
    //public RotateMode mode;  //виды вращения, содержит 4 значения
    //public AxisConstraint axisConstraint; //указывает где верх
    //public PathType pathType;  //Quaternions
    //public PathMode pathMode;
    public LoopType loopType;

    //public int numJumps;
    //public int resolution;
    //public int vibrato;
    public int times;
    
    //public float jumpPower;
    //public float endValue;
    public float duration;
    public float anotherDuration;

    //public float toStartWidth;
    //public float toEndWidth;
    //public float to;
    //public float elasticity;
    //public float strenght;
    //public float randomness;


    
    public bool snapping;
    //public bool satableZRotation;
    //public bool fadeOut;

    
    

    private void Start()
    {

        #region Движение
        //transform.DOMove(target.position, duration, snapping);  //Передвижение по всем трем осям к указанному объекту (в указанную точку) 
        //transform.DOMoveZ(endValue, duration, snapping);   //Передвижение по одной оси (DOMoveХ/DOMoveУ/DOMoveZ) к указанному значению показателя оси
        //transform.DOJump(target.position, jumpPower, numJumps,duration, snapping); //Прыжочек =)
        
        //Так же есть отдельные методы для ЛокалТрансформ, например ДОМувЛокал,ДОМувХЛокал и тд
        #endregion Движение
        
        #region Вращение
        //transform.DORotate(target.position, duration, mode); //Вращение объекта до параметров, указанных как цель
        //transform.DOLookAt(target.position, duration, axisConstraint);  //Поворачивается сторной Up (выбирать с помощиью AxisConstraint) в сторону выбранного объекта. Можно выбирать какая именно сторона будет UP
        #endregion Вращение

        #region Путь
        //transform.DOPath(waypoints, duration, pathType, pathMode, resolution, gizmoColor); //Передвигает объект между установленными точками. Есть разные настройки. Так же есть .SetLookAt(_target) смотреть на цель
        // так же есть разные настройки пути,снэппинг,локал путь и тд
        #endregion Путь

        #region Цвет
        //sprite.DOColor(gizmoColor, duration); //Меняет (подводит) цвет к определенному
        //sprite.DOFade(endValue, duration);  //Меняет альфа канал, можно не использовать,а вместо него использовать просто ДОКалор
        //sprite.DOGradientColor(gradient, duration); //Меняет цвет по градиенту,забавная хуйня))
        //sprite.DOBlendableColor(gizmoColor, duration); //Подстраивает цвет цели к заданному значению таким образом, чтобы другие анимации DOBlendableColor могли работать вместе над одной и той же целью, а не сражаться друг с другом, как это делали бы несколько DOColor.
        #endregion Цвет

        #region Трэил
        //trail.DOResize(toStartWidth, toEndWidth, _duration); //Меняя толщину шлейфа
        //trail.DOTime(to, duration); //работает с переменной Тайм компонента ТрейлРендерер, не до конца понятно что делает
        #endregion

        #region Скейл
        //target.DOScale(to, duration); //скейлит таргет по всем трем осям
        //target.DOScaleX(to, duration); //скейлит таргет по выбранной оси DOScaleX/DOScaleY/DOScaleZ
        #endregion

        #region Тряска
        //target.DOPunchPosition(punch, _duration, vibrato, elasticity,snapping); //тресет обект вперед назад по оси между стартовой позицией и указанной точкой Вектор3
        //target.DOPunchRotation(punch, _duration, vibrato, elasticity); //тресет объект по ротэйшену
        //target.DOPunchScale(punch, _duration, vibrato, elasticity); //тресет объект скейля туда-обратно
        
        //кароч делает одно и тоже,только используя разные поля компонента трансформ (позиция,ротация,скейл)
        #endregion

        #region Шэйк
        //target.DOShakePosition(duration, strenght, vibrato, randomness, snapping, fadeOut);
        //target.DOShakeRotation(duration, strenght, vibrato, randomness, fadeOut);
        //target.DOShakeScale(duration, strenght, vibrato, randomness, fadeOut);

        //эти методы очень похожи на методы тряски, тоже трясут обект по полям трансформа (по позиции/ротации/скейлу) но тут есть fadeOut,если его выключить,то объект будет все время тряститись) 
        #endregion
        
        transform.DOBlendableMoveBy(by, duration);
        transform.DOBlendableMoveBy(anotherBy, anotherDuration).SetLoops(times, loopType);


    }

    private void Update()
    {
        
    }

}
